import torch
import torch.nn as nn
import librosa
import pandas as pd
import numpy as np

clip_durations = pd.read_csv('clip_durations.tsv', sep='t')
invalidated = pd.read_csv('invalidated.tsv', sep='t')
other = pd.read_csv('other.tsv', sep='t')
reported = pd.read_csv('reported.tsv', sep='t')
unvalidated_sentences = pd.read_csv('unvalidated_sentences.tsv', sep='t')
validated = pd.read_csv('validated.tsv', sep='t')
validated_sentences = pd.read_csv('validated_sentences.tsv', sep='t')

audio_data = []
labels = []


def get_label_for_clip(clip_name, validated, validated_sentences):
    if clip_name in validated['clip'].values and clip_name in validated_sentences['clip'].values:
        return validated[validated['clip'] == clip_name]['label'].values[0]
    else:
        return 'Unknown'


for clip in os.listdir('clips'):
    Y, sr = librosa.load('clips'+clip)
    audio_data.append(librosa.feature.mfoc(Y, sr=sr))

    label = get_label_for_clip(clip, validated, validated_sentences)
    labels.append(label)

audio_data = np.array(audio_data)
labels = np.array(labels)



class VoiceRecognitionModel(nn.Module):
    def __init__(self):
        super(VoiceRecognitionModel, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=3, stride=1, padding=1)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1)
        self.fc1 = nn.Linear(64 * 4 * 4, 64)
        self.fc2 = nn.Linear(64, 10)

    def forward(self, x):
        x = self.conv1(x)
        x = torch.relu(x)
        x = self.maxpool(x)
        x = self.conv2(x)
        x = torch.relu(x)
        x = self.maxpool(x)
        x = self.conv3(x)
        x = torch.relu(x)
        x = x.view(-1, 64 * 4 * 4)
        x = self.fc1(x)
        x = torch.relu(x)
        x = self.fc2(x)
        return x


model = VoiceRecognitionModel()
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=0.001)

for epoch in range(10):
    for i, (inputs, labels) in enumerate(zip(audio_data, labels)):
        inputs = torch.from_numpy(inputs).unsqueeze(1).float()
        labels = torch.tensor(labels, dtype=torch.long)

        outputs = model(inputs)
        loss = criterion(outputs, labels)

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if i % 100 == 0:
            print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'
                  .format(epoch + 1, 10, i + 1, len(audio_data), loss.item()))

torch.save(model, 'voice_recognition_model.pt')