
import pandas as pd
import numpy as np
import os
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Flatten
import librosa

clip_durations = pd.read_csv('clip_durations.tsv', sep='t')
invalidated = pd.read_csv('invalidated.tsv', sep='t')
other = pd.read_csv('other.tsv', sep='t')
reported = pd.read_csv('reported.tsv', sep='t')
unvalidated_sentences = pd.read_csv('unvalidated_sentences.tsv', sep='t')
validated = pd.read_csv('validated.tsv', sep='t')
validated_sentences = pd.read_csv('validated_sentences.tsv', sep='t')

audio_data = []
labels = []


def get_label_for_clip(clip_name, validated, validated_sentences):
    if clip_name in validated['clip'].values and clip_name in validated_sentences['clip'].values:
        return validated[validated['clip'] == clip_name]['label'].values[0]
    else:
        return 'Unknown'


for clip in os.listdir('clips'):
    Y, sr = librosa.load('clips'+clip)
    audio_data.append(librosa.feature.mfoc(Y, sr=sr))

    label = get_label_for_clip(clip, validated, validated_sentences)
    labels.append(label)

audio_data = np.array(audio_data)
labels = np.array(labels)


model = Sequential()
model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(audio_data.shape[1], audio_data.shape[2], 1)))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(Flatten())
model.add(Dense(64, activation='relu'))
model.add(Dense(10))

model.compile(optimizer='adam',
              loss=keras.losses.SparseCategoricalCrossentropy(from_logits=True),
              metrics=['accuracy'])

model.fit(audio_data, labels, epochs=10)

model.save('voice_recognition_model.h5')

